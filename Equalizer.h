//
// Created by leonardo on 19/03/18.
//

#include "Image.h"
#include <string>
#include <opencv2/opencv.hpp>
#ifndef OPENMPEQUALIZATION_EQUALIZER_H
#define OPENMPEQUALIZATION_EQUALIZER_H

using namespace std;


class Equalizer {
public:
    Image equalize(Image* img);
    Image logTransform(Image img);
    Image gammaTransform(Image img, float gamma);
    int getMax(unsigned char* array, int size);
};


#endif //OPENMPEQUALIZATION_EQUALIZER_H
