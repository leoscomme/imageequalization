//
// Created by leonardo on 20/03/18.
//

#include <string>
#include <vector>
#include <opencv2/opencv.hpp>

#ifndef OPENMPEQUALIZATION_IMAGE_H
#define OPENMPEQUALIZATION_IMAGE_H

using namespace std;

class Image {
private:
    vector<cv::Mat> channels;
    vector <int*> histogram;
    vector<int*> cumulativeHistogram;
    int width,height;
    bool BGR;
public:
    Image(string path, bool BGR); // crea oggetto da un path
    Image(cv::Mat* image,bool BGR); // crea oggettotramite la matrice di una img
    Image(vector<unsigned char*> channels,int height, int width); // e equalizer restituisce una Image sfruttando il costruttore <-
    ~Image();

    vector<int*> *getHistogram(); //lazy load
    int* computeSinglePlaneHistogram(int index);

    vector <int*> *getCumulativeHistogram(); //lazy load
    int * getSinglePlaneCumulativeHistogram(int index);

    void saveImg(string path); // salva immagine
    void showImg(); // crea il mat e ci fa imshow
    void showHist();

    vector<unsigned char*>* getChannels();
    int getWidth();
    int getHeight();
    bool isBGR(){return BGR;}//FIXME const
};



#endif //OPENMPEQUALIZATION_IMAGE_H
