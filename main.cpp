#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/imgproc.hpp>
#include "Equalizer.h"
#include "Image.h"
using namespace std;
using namespace cv;

int main() {


    Equalizer myEq1;

    Image myImg("/home/cosimo/Scrivania/Lenna.png",true);


    Image logImg = myEq1.logTransform(myImg);
    logImg.showImg();

    Image gammaImg = myEq1.gammaTransform(myImg,3);
    gammaImg.showImg();


    Image eqImg = myEq1.equalize(&myImg);
    eqImg.showImg();


  /*  vector<unsigned char*>* ptr;
    ptr = myImg.getChannels();

    Image newImg = Image(*ptr,myImg.getHeight(),myImg.getWidth());
    newImg.showImg();
    newImg.saveImg("/home/leonardo/Scrivania/newImg.png");


    vector<Mat> channelsT;
    for(int i=0;i<ptr->size();++i){
        Mat ch(myImg.getHeight(), myImg.getWidth(), CV_8UC1, (*ptr)[i]);
        channelsT.push_back(ch);
    }
    Mat finaleImgT;
    merge(channelsT,finaleImgT);
    imwrite("/home/leonardo/Scrivania/ch1t.png",finaleImgT);


    Mat B(myImg.getHeight(), myImg.getWidth(), CV_8UC1, (*ptr)[0]);
    Mat G(myImg.getHeight(), myImg.getWidth(), CV_8UC1, (*ptr)[1]);
    Mat R(myImg.getHeight(), myImg.getWidth(), CV_8UC1, (*ptr)[2]);

    vector<Mat> channels;

    channels.push_back(B);
    channels.push_back(G);
    channels.push_back(R);

    Mat finalImage;
    merge(channels,finalImage);
    imwrite("/home/leonardo/Scrivania/ch1.png",finalImage);

    myImg.saveImg("/home/leonardo/Scrivania/saveIm.png");



    unsigned char buffer[image.rows][image.cols];

    uchar* p;
    for (int i = 0; i < image.rows; ++i) {
        p = image.ptr<uchar>(i);
        for (int j = 0; j < image.cols; ++j) {
            buffer[i][j] = p[j];
        }
    }


    for(int y=0;y<image.rows;y++)
    {
        for(int x=0;x<image.cols;x++)
        {
            // get pixel
            uchar color = image.at<uchar>(Point(x,y));

            // ... do something to the color ....

            // set pixel
            image.at<uchar>(Point(x,y)) = color;
        }
    }



    namedWindow( "Display window", WINDOW_AUTOSIZE );// Create a window for display.
    imshow( "Display window", image );

    waitKey(0);                                          // Wait for a keystroke in the window
    return 0;
     */
}