//
// Created by leonardo on 20/03/18.
//

#include "Image.h"

using namespace std;

Image::Image(string path, bool BGR) {
    cv::Mat image;
    if(BGR==true)
        image = cv::imread(path, CV_LOAD_IMAGE_UNCHANGED);
    else
        image = cv::imread(path, CV_LOAD_IMAGE_GRAYSCALE);

    if(! image.data )                              // Check for invalid input
    {
        cerr <<  "Could not open or find the image" << endl ;
        //Image::~Image();
    }
    else{
        this->BGR = BGR;
        this->height = image.rows;
        this->width = image.cols;

        if(BGR== true) {
            cv::split(image,this->channels);
        }
        else{
            this->channels.push_back(image);
        }


    }
}

Image::Image(vector<unsigned char *> channels, int height, int width) {
    for(int i=0;i<channels.size();++i){
        cv::Mat ch(height, width, CV_8UC1, channels[i]);
        this->channels.push_back(ch);
    }
    if(channels.size()==3)
        this->BGR = true;
    else
        this->BGR = false;
    this->height = height;
    this->width = width;

}



Image::Image(cv::Mat *image,bool BGR) {
    this->BGR = BGR;
    this->height = (*image).rows;
    this->width = (*image).cols;

    if(BGR== true) {
        cv::split(*image,this->channels);
    }
    else{
        this->channels.push_back(*image);
    }

}

Image::~Image() { //TODO ricontrollare se vect<myclass> non ha bisogno di distruttore (vector<myclass*> ha bisogno)

}

vector <int *>* Image::getHistogram() {
    if (histogram.empty()) {
        for (int i = 0; i < channels.size(); i++) {
            histogram.push_back(computeSinglePlaneHistogram(i));
        }
    }
    return new vector<int*>(histogram);
}




int* Image::computeSinglePlaneHistogram(int index) {
    int *tempHist= new int [256];
    for(int i=0; i< 256; i++){
        tempHist[i]=0;
    }
    for (int i=0; i<this->height; i++){
        uchar* p = channels[index].ptr<uchar>(i); // puntatore alla riga i esima dell''oggetto data dentro l'index-esimo Mat
        for (int j=0; j<this->width; j++){
            tempHist[(int)p[j]]++;//  non si può accedere con le doppie parentesi
        }
    }

    return tempHist;

}


vector <int*> *Image::getCumulativeHistogram(){
    if(cumulativeHistogram.empty()){
        for(int i=0; i<channels.size(); i++){
            cumulativeHistogram.push_back(getSinglePlaneCumulativeHistogram(i));
        }

    }
    return new vector <int *> (cumulativeHistogram);
}


int * Image::getSinglePlaneCumulativeHistogram(int index) {
    int * tempCumHist=new int [256];
    vector<int*>* hist = getHistogram();
    tempCumHist[0]=(*hist)[index][0];
    for(int i=1; i<256; i++){
        tempCumHist[i]=tempCumHist[i-1]+ (*hist)[index][i];
    }
    return tempCumHist;
}

vector<unsigned char*>* Image::getChannels() {
    vector<unsigned char*> dataChannels;
    for(auto channel = this->channels.begin(); channel!=this->channels.end(); ++channel){
        dataChannels.push_back(channel->data);
    }
    return new vector<unsigned char*>(dataChannels);
}

void Image::saveImg(string path) {
    cv::Mat finalImg;
    cv::merge(this->channels,finalImg);
    cv::imwrite(path,finalImg);
}

void Image::showImg() { //TODO test
    cv::Mat finalImg;
    cv::merge(this->channels,finalImg);

    cv::namedWindow( "Display window", cv::WINDOW_AUTOSIZE );// Create a window for display.
    cv::imshow( "Display window", finalImg );

    cv::waitKey(0);
}

int Image::getHeight() {
    return this->height;
}

int Image::getWidth() {
    return this->width;
}