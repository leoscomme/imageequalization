//
// Created by leonardo on 19/03/18.
//

#include <math.h>
#include <algorithm>
#include "Equalizer.h"




Image Equalizer::equalize(Image* img){
    vector<unsigned char*> *channels=img->getChannels();
    vector<unsigned char*> newChannels;
    int size=img->getWidth()*img->getHeight();
    vector<int *> * cumHist=img->getCumulativeHistogram();
    for(int i=0; i<channels->size(); i++){
        unsigned char *eqChannel=new unsigned char[size];
        for(int j=0; j<size; j++){
            int index=(*channels)[i][j];
            eqChannel[j]=(unsigned char)((*cumHist)[i][index]*255/float(size));
        }
        newChannels.push_back(eqChannel);

    }
    return  Image(newChannels,img->getHeight(),img->getWidth());

}



Image Equalizer::logTransform(Image img) {
    vector<unsigned char*>* channels; //TODO ha senso usare un puntatore?
    channels = img.getChannels();
    vector<unsigned char*> newChannels;
    int size = img.getWidth()*img.getHeight();
    for(auto channel : *channels){
        unsigned char* eqChannel = new unsigned char[size];
        double c = 255/(log(1+getMax(channel,size)));
        for(int i=0; i<size; ++i){
            eqChannel[i] = (unsigned char)c*log(1+int(channel[i]));
        }
        newChannels.push_back(eqChannel);
    }
    return  Image(newChannels,img.getHeight(),img.getWidth());
}

Image Equalizer::gammaTransform(Image img,float gamma) {
    vector<unsigned char*>* channels;
    channels = img.getChannels();
    vector<unsigned char*> newChannels;
    int size = img.getWidth()*img.getHeight();
    for(auto channel : *channels){
        unsigned char* eqChannel = new unsigned char[size];
        for(int i=0; i<size; ++i){
            eqChannel[i] = 255*pow((float)channel[i]/255,gamma);
        }
        newChannels.push_back(eqChannel);
    }
    return  Image(newChannels,img.getHeight(),img.getWidth());
}


int Equalizer::getMax(unsigned char* array, int size) {
    int max = 0;
    for(int i=0; i<size; ++i){
        if((int)array[i]>max)
            max = (int)array[i];
    }
    return max;
}

